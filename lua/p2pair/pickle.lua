-- for all functions: return code is default unix error codes: 0 is no error, 1 is error
local util = require("p2pair.util")
local pickle = {}

-- for debugging
function _G.dump(...)
    local objects = vim.tbl_map(vim.inspect, {...})
    print(unpack(objects))
end

function pickle.read_buf(bufhandle)
    -- set the bufhandle to the current one per default
    bufhandle = bufhandle == nil and 0 or bufhandle
    local api = vim.api

    -- if buffer(bufhandle) is focused get the cursorpos
    -- else send nil to indicate off-focus
    local cur_pos
    local cur_buf = api.nvim_win_get_buf(0)
    --if bufhandle == cur_buf or bufhandle == 0 then
        --cur_pos = api.nvim_win_get_cursor(0)
    --end

    -- get just the file name, not the whole file path
    local name = vim.split(vim.api.nvim_buf_get_name(0), "/")
    name = name[#name]

    local lines = api.nvim_buf_get_lines(bufhandle, 0, -1, true)

    return {
        name = name,
        lines = lines,
        cursorpos = cur_pos,
    }
end

function pickle.write_buf(bufhandle, contents)
    local api = vim.api
    bufhandle = bufhandle == nil and 0 or bufhandle

    -- if buffer(bufhandle) is focused and cursorpos isnt nil set the cursorpos
    local update_cursor = false
    local cur_buf = api.nvim_win_get_buf(0)

    --if bufhandle == cur_buf or bufhandle == 0 then
    --    update_cursor = true
    --end

    if contents then
        api.nvim_buf_set_name(bufhandle, contents.name)
        api.nvim_buf_set_lines(bufhandle, 0, -1, true, contents.lines)
        if contents.cursorpos and update_cursor then
            api.nvim_win_set_cursor(0, contents.cursorpos)
        end
        return 0
    else
        return 1
    end
end

function pickle.read_from_bchange(lines, bhandle, changedtick, fidx, lidx, last_line, bcount)
    -- TODO only transmit the changed lines instead of the whole buffer
    return pickle.read_buf(bhandle)
end

return pickle
