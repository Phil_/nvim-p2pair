-- this module attempts to enable pair programming via a vim<->vim p2p connection
local pickle = require("p2pair.pickle")
local client = require("p2pair.client")
local util = require("p2pair.util")
local p2pair = {}

-- for debugging
function _G.dump(...)
    local objects = vim.tbl_map(vim.inspect, {...})
    print(unpack(objects))
end


-- sets up the connection, then starts the vim event loop to synchronize the buffers
-- synchronize on cursor and buffer events -> subscribe with vim.loop
function p2pair.pair_connect(sync_buf, args)
    -- host, port, buffer for the synchronization
    local host = args.host ~= nil and args.host or "127.0.0.1"
    local port = args.port ~= nil and args.port or 0
    local thost = args.thost ~= nil and args.thost or "127.0.0.1"
    local tport = args.tport ~= nil and args.tport or 0
    sync_buf = sync_buf ~= nil and sync_buf or vim.api.nvim_create_buf(true, false)

    local dont_send = false

    local sock = client.connect_peer(host, port, thost, tport)
    sock:recv_start(function(_, data, _, _)
        if type(data) == "string" then
            data = util.stringToTable(data)
            vim.schedule(function()
                pickle.write_buf(sync_buf, data)
                dont_send = true
            end)
        end
    end)

    -- and write buffer changes
    vim.api.nvim_buf_attach(sync_buf, true, {
        on_lines = function(...)
            if not dont_send then
                local buf = pickle.read_from_bchange({...})
                buf.sockname = sock:getsockname()
                sock:send(util.tableToString(buf), nil, nil)
            end
            dont_send = false
        end
    })
end

function p2pair.pair_init(sync_buf, args)
    local host = args.host ~= nil and args.host or "127.0.0.1"
    local port = args.port ~= nil and args.port or 0
    local sock = client.create_peer(host, port)
    sync_buf = sync_buf ~= nil and sync_buf or vim.api.nvim_create_buf(true, false)

    local tport, thost
    local dont_send = false

    print("Opening on port "..sock:getsockname().port)

    -- receive the buffer updates
    sock:recv_start(function(_, data, _, _)
        if type(data) == "string" then
            vim.schedule(function()
                data = util.stringToTable(data)
                if thost == nil or tport == nil then
                    thost = data.sockname.ip
                    tport = data.sockname.port
                    sock:bind(thost, tport)
                end
                pickle.write_buf(sync_buf, data)
                dont_send = true
            end)
        end
    end)

    -- and send buffer changes too
    vim.api.nvim_buf_attach(sync_buf, true, {
        on_lines = function(...)
            if thost ~= nil and tport ~= nil then
                if not dont_send then
                    local buf = pickle.read_from_bchange({...})
                    buf.sockname = sock:getsockname()
                    local str = util.tableToString(buf)
                    sock:send(str, thost, tport)
                end
                dont_send = false
            end
        end
    })
end

return p2pair
