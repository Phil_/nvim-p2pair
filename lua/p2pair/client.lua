local client = {}
local uv = vim.loop

-- ################################################

-- default values
local d_host = "127.0.0.1"
local d_port = 0

local d_thost = "127.0.0.1"
local d_tport = 33490

-- ################################################

-- use the tcp as a uv_stream_t for p2p or as uv_tcp_t for server/client
function client.create_peer(host, port)
    host = host ~= nil and host or d_host
    port = port ~= nil and port or d_port

    local peer = uv.new_udp()
    peer:bind(host, port)
    return peer
end

function client.connect_peer(host, port, thost, tport)
    thost = thost ~= nil and thost or d_thost
    tport = tport ~= nil and tport or d_tport

    local peer = client.create_peer(host, port)
    peer:connect(thost, tport)
    return peer
end

return client
