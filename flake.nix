{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }@inputs:
  flake-utils.lib.eachDefaultSystem (system: let  
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [
        luajit
        sumneko-lua-language-server
      ];
      shellHook = ''
        export LUA_PATH="$LUA_PATH;./lua/?.lua;./lua/?/init.lua"
      '';
    };
  });
}
